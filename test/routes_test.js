const chai = require('chai')
const expect = chai.expect
// or const { expect } = require('chai')
const http = require('chai-http')

chai.use(http);

describe('API Test Suite', () => {
	it('Test API Get people is running', () => {
		chai.request('http://localhost:5001').get('/people')
		.end((err, res) => {
			expect(res).to.not.equal(undefined)
		})
	})

	it('Test API get people returns 200', () => {
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200)
		})
	})

    it('Test API post person returns 400 if no NAME property', () => {
        console.log("TEST")
		chai.request('http://localhost:5001')
            .post('/person')
            .type('json')
            .send({
                alias: "Mikasa",
                age: 27
            })
            .end((err, res) => {
                console.log(res)
                console.log(res.status)
                expect(res.status).to.equal(400)
		})

	})

	it('[1] Check if Person endpoint is running', () => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Colossal Titan",
			name: "Armin",
			age: 28
		})
		.end((err, res) => {
			// console.log(res.status);
			expect(res.status).to.not.equal(undefined);
		})
	})

	it('[2] Test API post person returns 400 if no ALIAS', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name: "Ackerman",
			age: 27
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('[3] Test API post person returns 400 if no AGE', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Mikasa",
			name: "Ackerman",
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

})