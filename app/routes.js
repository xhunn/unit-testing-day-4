const res = require('express/lib/response');
const { names } = require('../src/util.js');

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({data: {}})
    })
    
    app.get('/people', (req, res) => {
        return res.send({
            people: names
        })
    })

    app.post('/person', (req, response) => {
        if (!req.body.hasOwnProperty('name')) {
            console.log(response)
            return response.status(400).send({
                'error': 'Bad request: Missing required paramater NAME'
            })
        } 
        if (typeof req.body.name !== 'string') {
            return response.status(400).send({
                'error': 'Bad Request: NAME has to be string'
            })
        } 
        if (req.body.hasOwnProperty('age')) {
            return response.status(400).send({
                'error': 'Bad Request: missing required parameter AGE'
            })
        } 
        if (typeof req.body.age !== 'number') {
            return response.send(400).send({
                'error': 'Bad Request: AGE has to be a number'
            })
        }
        if (req.body.hasOwnProperty('alias')) {
            return response.status(400).send({
                'error': 'Bad Request: missing required parameter ALIAS'
            })
        } 
    })
 }