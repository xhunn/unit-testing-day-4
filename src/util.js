const factorial = (n) => {
    if (typeof n !== 'number') {
        return false;
    } else if (n === 0 || n === 1) {
        return 1
    } else if (n < 0) {
        return undefined;
    } else {
        return n * factorial(n-1)
    }
}

const divisibleBySeven = (number) => {
    if (number === 0){
        return false
    } else if (number % 7 === 0) {
        return true
    } else {
        return false
    }
}

const divisibleByFive = (number) => {
    if (number === 0){
        return false
    } else if (number % 5 === 0) {
        return true
    } else {
        return false
    } 
}

const names = {
	Eren : {
		"alias" : "Attack Titan",
		"name" : "Eren Yeager",
		"age": 28
	},
	Levi : {
		"alias" : "Clean Boi",
		"name" : "Levi Ackerman",
		"age" : 38
	}
};

module.exports = {
    names: names,
    factorial: factorial,
    divisibleByFive: divisibleByFive,
    divisibleBySeven: divisibleBySeven
}